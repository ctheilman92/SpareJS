var http = require('http');
var path = require('path');
var fs = require('fs');

/*
making simpler but adding filename extensions to handle .css and .js files in /public
*/



http.createServer(function (request, response) {

	var filePath = '.' + request.url;

	var localFolder = __dirname + '/public/';	//__dirname returns root folder which this js is in (Google Drive/progs2016/Nodejs/)
	//var assetsFolder = __dirname + '/public/assets/';
	var htmlStuff = localFolder + 'index.html';
	var styleStuff = localFolder + 'style.css';
	var staticimg = localFolder + '2324-lrg.jpg';

	var img1 = staticimg;
	var img2 = localFolder + '2324-lrg-01.jpg';
	var gif1 = localFolder + 'bg_line.gif';
	var gif2 = localFolder + 'bgBottom1.gif';
	var img3 = localFolder + 'bgWoman.jpg';
	var imgs = [img1, img2, img3];
	var gifs = [gif1, gif2];

	//to load html
	if (request.url.indexOf('.html') != -1) {
		console.log("loading html page: " + htmlStuff);
		fs.readFile(htmlStuff, function (err, content) {
			if (err) {
				response.writeHead(500);
				response.end();
			}
			else {
				response.writeHead(200, {'Content-Type': 'text/html'});
				response.end(content, 'utf-8');
			}
		});
	}

	//for jpg's
	if (request.url.indexOf('.jpg') != -1) {
		for (i = 0; i < imgs.length; i++) {
		    console.log("loading image: " + imgs[i])
			fs.readFile(imgs[i], function (err, content) {
				if (err) {
					response.writeHead(500);
					response.end();
				}
				else {
					response.writeHead(200, {'Content-Type': 'image/jpeg'});
					response.end(content, 'utf-8');
				}
			});
		}
	}

	//for gif's
	if (request.url.indexOf('.gif') != -1) {
		for (i = 0; i < gifs.length; i++) {
			console.log("loading gifs: " + gifs[i])
			fs.readFile(gifs[i], function (err, content) {
				if (err) {
					response.writeHead(500);
					response.end();
				}
				else {
					response.writeHead(200, {'Content-Type': 'image/gif'});
					response.end(content, 'utf-8');
				}
			});
		}
	}

	//to load js
	// if (request.url.indexOf('.js') != -1) {
	// 	fs.readFile(daFile, function (err, content) {
	// 		if (err) {
	// 			response.writeHead(500);
	// 			response.end();
	// 		}
	// 		else {
	// 			response.writeHead(200, {'Content-Type': 'text/javascript'});
	// 			response.end(content, 'utf-8');
	// 		}
	// 	});
	// }

	//to load cssmin
	if (request.url.indexOf('.css') != -1) {
		fs.readFile(styleStuff, function (err, content) {
			if (err) {
				response.writeHead(500);
				response.end();
			}
			else {
				response.writeHead(200, {'Content-Type': 'text/css'});
				response.end(content, 'utf-8');
			}
		});
	}



		console.log(request.url);
}).listen(3000);
console.log('Server running at http:localhost:3000/');
