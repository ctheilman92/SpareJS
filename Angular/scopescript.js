angular.module('scopeExample', [])
    .controller('MyController', ['$scope', function($scope) {

        $scope.username = '';
        $scope.defaultun = 'world';

        //get from input
        $scope.sayHello = function() {
            $scope.greeting = 'Hello ' + $scope.username + '!';
        };

        //default to world
        $scope.defaulted = function() {
            $scope.greeting = 'Hello ' + $scope.defaultun + '!';
        };
        
    }]);
