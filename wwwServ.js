var http = require('http');
	url = require('url');
	path = require('path');	//for file paths
	fs = require('fs');

/*
	set up to host "http://localhost:3000/index.html"
	static html through NODE
*/

//handle http requests
function requestHandler(req, res) {
	var content = '',
	fileName = path.basename(req.url),	//file that was requested
	localFolder = __dirname + '/public/';	//__dirname returns root folder which this js is in (Google Drive/progs2016/Nodejs/)

	if(fileName === 'index.html') {
		content = localFolder + fileName;	//file setup to be returned

		fs.readFile(content, function(err, contents) {
			if(!err) {
				res.end(contents);
				//send index.html contents and close request if readfile was successful
			}
			else {
				console.dir(err);
			};
		});
	}
	else {
		//if file was not found set 404 header...
		res.writeHead(404, {'Content-Type': 'text/html'});
		res.end('<h1>Sorry, the page you requested cannot be found.</h1>');
	};
};	//end requestHandler



http.createServer(requestHandler).listen(3000);
console.log("check 'http://localhost:3000/index.html'\n to see if the static page display")