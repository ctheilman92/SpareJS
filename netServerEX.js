var http = require('http');


//create http server to host hello world response. simple
var server = http.createServer(function (request, response) {
    response.writeHead(200, {'Content-Type': 'text/html'});
    response.write('<h1>Node.js</h1>');
    response.end('Hello world\n\n\n\n\n');
}).listen(8124);

console.log('Server Created & running @ http://127.0.0.1:8124');



//gracefully exiting anode js
process.on('SIGINT', function() {
	console.log('shutting down http server from SIGINT (ctrl+C)');

	server.close(function () { console.log('server closed!'); });
});
